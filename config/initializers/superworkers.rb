Superworker.define(:StopTaskWorkflow) do
  StopTasks() do
    parallel do
      PostToSalesForce()
      SendStopedTasksEmails()
    end
  end
end
