set :repo_url, 'git@bitbucket.org:itverezovskyi/tm2.git'
set :application, 'timemachine'
application = 'timemachine'
user = "deployer"
set :rvm_type, :user
set :rvm_ruby_version, '2.0.0-p598'
set :bundle_dir, "/usr/local/rvm/gems/ruby-2.0.0-p598"
set :deploy_to, '/var/www/apps/timemachine'
set :bundle_flags, ''
set :keep_releases, 3
set :pty, false
set :rails_env, "production"


namespace :foreman do
  desc 'Start server'
  task :start do
    on roles(:all) do
      sudo "start #{application}"
    end
  end

  desc 'Stop server'
  task :stop do
    on roles(:all) do
      sudo "stop #{application}"
    end
  end

  desc 'Restart server'
  task :restart do
    on roles(:all) do
      sudo "restart #{application}"
    end
  end

  desc 'Server status'
  task :status do
    on roles(:all) do
      execute "initctl list | grep #{application}"
    end
  end
end

namespace :deploy do
desc 'Setup'
  task :setup do
    on roles(:all) do

      puts "***********************************"
      puts "*********** Setup *****************"
      puts "***********************************"


      execute "mkdir -p  #{shared_path}/config/"
      execute "mkdir -p  #{shared_path}/log/"
      execute "mkdir -p /var/www/apps/#{application}/run/"
      execute "mkdir -p /var/www/apps/#{application}/log/"
      execute "mkdir -p /var/www/apps/#{application}/socket/"
      execute "mkdir -p #{shared_path}/system"

      upload!('shared/database.yml', "#{shared_path}/config/database.yml")

      upload!('shared/Procfile', "#{shared_path}/Procfile")

      upload!('shared/env.yml', "#{shared_path}/config/env.yml")

      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "db:reset"
        end
      end
    end
  end

  desc 'Create symlink'
  task :symlink do
    on roles(:all) do

      puts "***********************************"
      puts "****** Create symlink *************"
      puts "***********************************"


      execute "ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml"
      execute "ln -s #{shared_path}/config/env.yml #{release_path}/config/env.yml"
      execute "ln -s #{shared_path}/Procfile #{release_path}/Procfile"
      execute "ln -s #{shared_path}/system #{release_path}/public/system"
    end
  end

  desc 'Foreman init'
  task :foreman_init do
    on roles(:all) do

      puts "***********************************"
      puts "****** Init unicorn ***************"
      puts "***********************************"

      foreman_temp = "/var/www/tmp/foreman"
      execute  "mkdir -p #{foreman_temp}"

      execute "ln -s #{release_path} #{current_path}"

      within current_path do
        execute "cd #{current_path}"
        execute :bundle, "exec foreman export upstart #{foreman_temp} -a #{application} -e #{release_path}/.env.production -u deployer -l /var/www/apps/#{application}/log -d #{current_path}"
      end
      sudo "mv #{foreman_temp}/* /etc/init/"
      sudo "rm -r #{foreman_temp}"
    end
  end
end



  after 'deploy:finishing', 'deploy:cleanup'
  after 'deploy:finishing', 'deploy:restart'

  after 'deploy:updating', 'deploy:symlink'

  after 'deploy:setup', 'deploy:foreman_init'

  after 'deploy:foreman_init', 'foreman:start'

  before 'deploy:foreman_init', 'rvm:hook'

  before 'deploy:setup', 'deploy:starting'
  before 'deploy:setup', 'deploy:updating'
  before 'deploy:setup', 'bundler:install'
