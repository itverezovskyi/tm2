# Load the rails application
require File.expand_path('../application', __FILE__)
require File.join(Rails.root, 'lib', 'numeric.rb')
require File.join(Rails.root, 'lib', 'array.rb')

# Initialize the rails application
NetzkeDemo::Application.initialize!