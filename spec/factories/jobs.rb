# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :job do
    client nil
    SFID "MyString"
    show "MyString"
    booth_size nil
    sc nil
    onTablet false
  end
end
