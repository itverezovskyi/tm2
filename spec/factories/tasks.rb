# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :task do
    date "2014-07-31"
    startTime "2014-07-31 00:02:10"
    endTime "2014-07-31 00:02:10"
    job nil
    tech nil
    function nil
    onTablet false
  end
end
