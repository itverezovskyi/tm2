class ComponentsController < ApplicationController

  def home
    if Netzke::Base.controller.current_user then
      render :inline => "<% title 'TM', false %><%= netzke :application %>", :layout => true
    end
  end
end
