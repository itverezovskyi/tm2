class Api::V1::BaseController < ActionController::Base

  acts_as_token_authentication_handler_for User, fallback: :none
  before_filter :cors_preflight
  before_filter :require_authentication!

  def cors_preflight
    headers["Access-Control-Allow-Origin"] = "*"
    headers["Access-Control-Allow-Methods"] = 'GET,POST,PUT,DELETE,OPTIONS'
    headers["Access-Control-Allow-Headers"] = 'Origin,Accept,Content-Type'
    headers['Access-Control-Max-Age'] = "3628800"
    head(:ok) if request.request_method == "OPTIONS"
  end

  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  def not_found
    return api_error(status: 404, errors: 'Not found')
  end

  def destroy_session
    request.session_options[:skip] = true
  end

  def api_error(status: 500, message: 'Not found.', errors: [])
    unless Rails.env.production?
      puts errors.full_messages if errors.respond_to? :full_messages
    end
    head status: status and return if errors.empty?

    render json: { message: message, errors: errors}, status: status
  end

  def require_authentication!
    return api_error(status: 401, message: 'Check your credentials', errors: 'Wrong credentials') unless current_user.presence
  end

end
