class Api::V1::JobsController < Api::V1::BaseController

  def show
    job = Job.find(params[:id])

    render(json: Api::V1::JobSerializer.new(job).to_json)
  end

  def autocomplete
    jobs = Job.order(:name).where("sfid LIKE ?", "'%#{params[:sfid]}%'").take(10)

    render(
      json: ActiveModel::ArraySerializer.new(
        jobs,
        each_serializer: Api::V1::JobSerializer
      )
    )
  end

  def index
    jobs = paginate Job.where("sfid LIKE ?", "%#{params[:sfid]}%").take(10)

    render(
      json: ActiveModel::ArraySerializer.new(
        jobs,
        each_serializer: Api::V1::JobSerializer,
      )
    )
  end

end
