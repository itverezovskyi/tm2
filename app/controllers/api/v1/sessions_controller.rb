class Api::V1::SessionsController < Devise::SessionsController

  acts_as_token_authentication_handler_for User, fallback: :none
  before_filter :cors_preflight

  protect_from_forgery with: :null_session
  before_action :destroy_session


  def cors_preflight
    headers["Access-Control-Allow-Origin"] = "*"
    headers["Access-Control-Allow-Methods"] = 'GET,POST,PUT,DELETE,OPTIONS'
    headers["Access-Control-Allow-Headers"] = 'Origin,Accept,Content-Type'
    headers['Access-Control-Max-Age'] = "3628800"
    head(:ok) if request.request_method == "OPTIONS"
  end

  def create
    resource = User.find_for_database_authentication(email: params[:email])
    return invalid_login_attempt unless resource
    if resource.valid_password?(params[:password])
      sign_in("user", resource)
      render json: {success: true, info: "Logged in", data: resource }
      return
    end
    invalid_login_attempt
  end

  def destroy
    warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
    current_user.update_column(:authentication_token, nil)
    render status: 200, json: { success: true, info: "Logged out", data: {} }
  end

protected

  def ensure_params_exist
    return unless params[:user].blank? or params[:user][:email].blank? or params[:user][:password].blank?
    render json: {success: false, errors: "missing user parameter"}, status: 422
  end

  def invalid_login_attempt
    warden.custom_failure!
    render json: {success: false, errors: "Wrong credentials"}, status: 401
  end

  def destroy_session
    request.session_options[:skip] = true
  end
end
