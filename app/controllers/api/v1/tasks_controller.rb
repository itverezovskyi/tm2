class Api::V1::TasksController < Api::V1::BaseController

  def show
    task = Task.find(params[:id])

    render(json: Api::V1::TaskSerializer.new(task).to_json)
  end

  def index
    tasks = paginate Task.all

    render(
      json: ActiveModel::ArraySerializer.new(
        tasks,
        each_serializer: Api::V1::TaskSerializer
      )
    )
  end

  def create
    task = create_new_task

    return api_error(status: 422, errors: "Wrong type of task !") unless task
    return api_error(status: 422, errors: task.errors) unless task.valid?
    task.save!

    current_user.update_attribute(:current_task, task.id)

    render(
      json: Api::V1::TaskSerializer.new(task).to_json,
      status: 201,
      location: api_v1_task_path(task.id)
    )
  end

  def update
    task = Task.find(params[:id])

    task.startTime = params[:startTime]
    task.endTime   = params[:endTime]
    task.note      = task.note.gsub(ENV["STOPPED_PHRASE"], "")

    return api_error(status: 422, errors: "Star is after end, check imput !") unless task.startTime < task.endTime
    return api_error(status: 422, errors: "Task can't last more then 10h !") unless ((task.endTime - task.startTime) / 1.hour).round <= 10
    task.save!

    render(
      json: Api::V1::TaskSerializer.new(task).to_json,
      status: 200,
      location: api_v1_task_path(task.id)
    )
  end

  def stop
    task = Task.find(params[:id])

    if task
      if task.endTime
        current_user.update_attribute(:current_task, nil)
        return api_error(status: 405, errors: "Task already stopped")
      elsif task.user != current_user
        return api_error(status: 409, errors: "You didn't start this task")
      else
        task.update(endTime: params[:endTime])
        return api_error(status: 422, errors: task.errors) unless task.valid?
        task.save!
        current_user.update_attribute(:current_task, nil)
      end
    else
      raise ActiveRecord::RecordNotFound
    end

    render(json: Api::V1::TaskSerializer.new(task).to_json)
  end

private

  def create_params
    params[:data].merge(user_id: current_user.id)
  end

  def create_new_task
    if params[:data][:type] == Task::TASK_TYPES[0]
      SfaTask.new(create_params)
    elsif params[:data][:type] == Task::TASK_TYPES[1]
      ShopTask.new(create_params)
    end
  end

end
