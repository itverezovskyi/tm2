class Api::V1::UsersController < Api::V1::BaseController

  def history
    tasks = Task.where("user_id = ?", current_user.id).where.not(endTime: nil).order('created_at DESC').limit(5)

    render(
      json: ActiveModel::ArraySerializer.new(
        tasks,
        each_serializer: Api::V1::TaskSerializer
      )
    )
  end

  def current
    current_task = User.find(params[:id]).current_task

    return api_error(status: 200, errors: "No task assigned") unless current_task

    render(json: Api::V1::TaskSerializer.new(Task.find(current_task)).to_json)
  end

end
