class ApplicationController < ActionController::Base
  protect_from_forgery
  helper :all
  before_action :authenticate_user!
end
