class StopTaskWorkflowWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  # schedule_time_zone "Central Time (US & Canada)"
  Time.zone = "Central Time (US & Canada)"

  recurrence { daily.hour_of_day(4) }

  def perform
    StopTaskWorkflow.perform_async()
  end

end
