class FetchAllSfIndexes
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  def perform
    update_sf_index
  end

  private

  def update_sf_index
    find_job_without_sf_index.each do |job|
      save_sf_index_to_db(job.sfid)
    end
  end

  def save_sf_index_to_db(sfid)
    sf_indexes = get_sf_index_by_sfid(sfid)
    if sf_indexes.size != 0
      Job.find_by(sfid: sfid).update(sf_indexes: sf_indexes)
      write_system_to_log(sfid)
    else
      write_system_to_log_error(sfid)
    end
  end

  def find_job_without_sf_index
    Job.all.select{|job| job.sf_indexes == []}
  end

  def get_sf_index_by_sfid(sfid)
    query = restforce_client.query(select_query(sfid))
    query.inject([]){ |res, object| res << object.Id }
  end

  def select_query(sfid)
    "select Id, Opp_SFID__c from Order where Opp_SFID__c = '#{sfid}' AND Order_Type__c = 'Workorder'"
  end

  def write_system_to_log_error(sfid)
    SystemLogger.error("Get index from SF", "There is no matching order in SF for job #{sfid}")
  end

  def write_system_to_log(sfid)
    SystemLogger.info("Get index from SF", "Index for #{sfid} has been fetched from SF")
  end

  protected

  def restforce_client
    @restforce_client ||= create_restforce_client
  end

  def write_to_log
  end

  def create_restforce_client
    Restforce.new host: ENV["SFDC_DOMAIN"],
      username:         ENV["SFDC_USERNAME"],
      password:         ENV["SFDC_PASSWORD"],
      security_token:   ENV["SFDC_SECURITY_TOKEN"],
      client_id:        ENV["SFDC_CLIENT_ID"],
      client_secret:    ENV["SFDC_CLIENT_SECRET"],
      api_version:      "32.0"
  end
end
