class OrdersSyncWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { minutely(30) }

  def perform
    orders = restforce_client.query("select Opp_SFID__c, Id, AccountId, Project_Manager__c, Configuration_Size__c, Order_Type__c, Order_Approved__c, SFA_Only__c, Show_Name__c, LastModifiedDate from Order where Order_Type__c = 'Workorder' AND Order_Approved__c = True AND LastModifiedDate > #{get_last_sf_sync_date}")

    orders.each do |order|
      job = create_new_job_or_update(order)
      if job.valid?
        job.save
        write_system_to_log(job)
      end
    end

    update_sync_date
  end

  private

  def create_new_job_or_update(order)
    Job.find_by(sfid: order.Opp_SFID__c) ? add_sf_index(order) : create_new_job(order)
  end

  def create_new_job(order)
    order.SFA_Only__c ? SfaJob.new(prepare_job(order)) : ShopJob.new(prepare_job(order))
  end

  def prepare_job(order)
    job = Hash.new
    job[:client_id] = get_client(order.AccountId)
    job[:sfid] = get_sfid(order.Opp_SFID__c)
    job[:show] = get_show_name(order.Show_Name__c)
    job[:booth_size_id] = get_boot_size(order.Configuration_Size__c)
    job[:project_manager] = order.Project_Manager__c
    job[:sf_indexes] = [].push(order.Id)
    job
  end

  def add_sf_index(order)
    job = Job.find_by(sfid: order.Opp_SFID__c)
    job[:sf_indexes] = job[:sf_indexes].push(order.Id) unless job[:sf_indexes].include? order.Id
    job
  end

  def get_boot_size(booth_size_from_sf)
    boot_size = booth_size_from_sf.blank? ? BoothSize.find(1) : BoothSize.find_or_create_by(name: booth_size_from_sf)
    boot_size.id
  end

  def get_sfid(sfid_from_sf)
    sfid_from_sf.blank? ? 0 : sfid_from_sf
  end

  def get_show_name(show_from_sf)
    Nokogiri::HTML.parse(show_from_sf).text
  end

  def get_client(client_from_sf)
    client = find_client(client_from_sf)
    client = client.blank? ? create_client(client_from_sf) : client
    client.id
  end

  def find_client(account_id)
    Client.find_by(account: account_id)
  end

  def create_client(account_from_sf)
    Client.create(name: query_client_from_sf(account_from_sf), account: account_from_sf)
  end

  def query_client_from_sf(account_from_sf)
    restforce_client.query("select Name from Account where Id = '#{account_from_sf}'").first.Name
  end

  def update_sync_date
    SfIntegration.find(1).update(last_update_time: DateTime.now)
  end

  def get_last_sf_sync_date
    SfIntegration.find(1).last_update_time.iso8601
  end

  def write_system_to_log(job)
    SystemLogger.info("Fetch from SF", "Job #{job.sfid} for #{job.client.name} has been fetched from SF")
  end

  protected

  def restforce_client
    @restforce_client ||= create_restforce_client
  end

  def create_restforce_client
    Restforce.new host: ENV["SFDC_DOMAIN"],
      username:         ENV["SFDC_USERNAME"],
      password:         ENV["SFDC_PASSWORD"],
      security_token:   ENV["SFDC_SECURITY_TOKEN"],
      client_id:        ENV["SFDC_CLIENT_ID"],
      client_secret:    ENV["SFDC_CLIENT_SECRET"],
      api_version:      "32.0"
  end
end
