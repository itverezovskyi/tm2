class SendStopedTasksEmails
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  def perform
    send_emails
  end

  private

  def send_emails
    find_stopped_tasks.each { |task| send_task_email(task) }
  end

  def find_stopped_tasks
    select_tasks_with_note.select { |task| task[:note].include? ENV["STOPPED_PHRASE"] }
  end

  def select_tasks_with_note
    find_today_tasks.select { |task| task[:note] }
  end

  def send_task_email(task)
    AppMailer.stoped_task_notification(task).deliver_now
  end

  def find_today_tasks
    Task.where("updated_at >= ?", 10.days.ago.beginning_of_day)
  end

  def write_to_system_log(task)
    SystemLogger.info("Send email", "Email to notify #{task.user.name} that times for task #{task.id} in #{task.job.sfid} needed to be corrected has been send")
  end

end
