class StopTasks
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  def perform
    finish_tasks
  end

  private

  def finish_tasks
    find_not_finished_tasks.each do |task|
      update_time_and_set_note(task)
      remove_active_current_task(task)
      write_to_system_log(task)
    end
  end

  def remove_active_current_task(task)
    if task.id == task.user.current_task
      task.user.update_attribute(:current_task, nil)
    end
  end

  def update_time_and_set_note(task)
    task.endTime = task.startTime + 4.hours
    task.note    = task.note + " " + ENV["STOPPED_PHRASE"]
    task.save!
  end

  def find_not_finished_tasks
    Task.where(endTime: nil)
  end

  def write_to_system_log(task)
    SystemLogger.info("Stop task", "Task #{task.id} started by #{task.user.name} has been stoped")
  end

end
