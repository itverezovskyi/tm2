class PostToSalesForce
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  def perform
    send_data_to_sf
  end

  private

  def send_data_to_sf
    total_time_for_jobs.each do |job|
      job[:sf_indexes].each do |index|
        update_order(index, job)
        write_system_to_log(job)
      end
    end
  end

  def update_order(id, data)
    rise_update_error(id, data) unless restforce_client.update('Order', Id: id, Services_Times_I_D__c: data["I&D"])
    rise_update_error(id, data) unless restforce_client.update('Order', Id: id, Services_Times_Pre_Stage__c: data["PreStage"])
    rise_update_error(id, data) unless restforce_client.update('Order', Id: id, Services_Time_Return_Inspection__c: data["Return inspection"])
  end

  def find_changed_jobs
    find_changed_tasks.inject([]) { |res, task| res.push(task.job) }.uniq.compact
  end

  def find_changed_tasks
    Task.where("updated_at >= ?", 3.days.ago.beginning_of_day)
  end

  def find_changed_jobs_with_sf_index
    find_changed_jobs.delete_if { |job| job.sf_indexes.empty? }
  end

  def total_time_for_jobs
    find_changed_jobs_with_sf_index.map{ |job| hash_with_job_data(job) }
  end

  def hash_with_job_data(job)
    Hash.[](:sfid, job.sfid, :sf_indexes, job.sf_indexes).merge(calc_total_time_for_job(job))
  end

  def calc_total_time_for_job(job)
    Phase.all.inject({}) { |res, phase| res.merge(hash_of_phase_time(job, phase)) }
  end

  def hash_of_phase_time(job, phase)
    Hash.[](phase.name, round_to_quarter(calc_total_time_in_phase(job, phase)))
  end

  def calc_total_time_in_phase(job, phase)
    calc_time_diff(find_tasks_by_job_and_phase(job, phase))
  end

  def calc_time_diff(tasks)
    tasks.inject(0) { |sum, task| sum += task.endTime - task.startTime }
  end

  def find_tasks_by_job_and_phase(job, phase)
    Task.where(job_id: job.id, phase_id: phase.id)
  end

  def round_to_quarter(time_difference)
    full_minutes = (time_difference / 60).floor
    hours = (full_minutes / 60).floor
    minutes = (((full_minutes - (hours * 60)) / 15.0).ceil) * 25
    return "#{hours += 1}" unless minutes != 100
    "#{hours}.#{minutes}"
  end

  def rise_update_error(id, data)
    SystemLogger.error("Post to SF", "Error, while updating #{id} with #{data}")
    raise "Error, while updating #{id} with #{data}"
  end

  def write_system_to_log(job)
    SystemLogger.info("Post to SF", "#{job[:sfid]} with SF index #{job[:sf_indexes].first} has been updated")
  end

  protected

  def restforce_client
    @restforce_client ||= create_restforce_client
  end

  def create_restforce_client
    Restforce.new host: ENV["SFDC_DOMAIN"],
      username:         ENV["SFDC_USERNAME"],
      password:         ENV["SFDC_PASSWORD"],
      security_token:   ENV["SFDC_SECURITY_TOKEN"],
      client_id:        ENV["SFDC_CLIENT_ID"],
      client_secret:    ENV["SFDC_CLIENT_SECRET"],
      api_version:      "32.0"
  end
end
