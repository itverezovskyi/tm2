class Api::V1::TaskSerializer < Api::V1::BaseSerializer
  attributes :id, :date, :startTime, :endTime, :job_id, :sfid, :client, :note, :phase_id, :created_at, :updated_at

  def client
    Job.find(object.job_id).client.name
  end

  def sfid
    Job.find(object.job_id).sfid
  end

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end
end