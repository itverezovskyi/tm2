class Api::V1::JobSerializer < Api::V1::BaseSerializer
  attributes :id, :show, :sfid, :client, :booth_size, :project_manager, :type, :created_at, :updated_at


  def client
    object.client.name
  end

  def booth_size
    object.booth_size.name
  end

  def project_manager
    object.project_manager
  end

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end
end
