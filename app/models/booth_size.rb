class BoothSize < ActiveRecord::Base
  has_many :job

  attr_accessible :name
  validates_presence_of :name
end
