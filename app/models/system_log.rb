class SystemLog < ActiveRecord::Base
  attr_accessible :level, :job, :action
  validates_presence_of :level, :action
end
