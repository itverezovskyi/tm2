class Phase < ActiveRecord::Base
  has_many :task

  attr_accessible :name
  validates_presence_of :name
  validates_uniqueness_of :name
end
