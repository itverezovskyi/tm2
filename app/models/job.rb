class Job < ActiveRecord::Base
  belongs_to :client
  belongs_to :booth_size
  has_many :task

  attr_accessible :client_id, :sfid, :show, :booth_size_id, :project_manager, :type, :sf_indexes
  validates_presence_of :client, :sfid, :show, :booth_size, :project_manager, :type
  validates_uniqueness_of :sfid

  def self.calculate_total(job_id = id)
    total = 0
    tasks = Task.where(job_id: job_id).where.not(endTime: nil)
    tasks.each do |task|
      total += task.endTime - task.startTime
    end
    total.to_time_difference
  end

  def total_time
  end

  def pre_stage_total
  end

  def i_and_d_total
  end

  def ri_total
  end

end
