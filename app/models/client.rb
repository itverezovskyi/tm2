class Client < ActiveRecord::Base
  has_many :job

  attr_accessible :name, :account
  validates_presence_of :name
  validates_uniqueness_of :name
end
