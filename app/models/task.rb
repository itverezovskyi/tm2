class Task < ActiveRecord::Base

  TASK_TYPES = %w(SfaTask ShopTask)

  belongs_to :job
  belongs_to :user
  belongs_to :phase

  attr_accessible :date, :startTime, :endTime, :type, :job_id, :user_id, :note, :phase_id
  validates_presence_of :date, :startTime, :type, :job, :user, :phase

  validates :date, date: true
  validates :startTime, date: true
  validates :endTime, date: { allow_blank: true, after: :startTime, message: 'must be after start' }

  def time_spended
  	(endTime - startTime).to_time_difference
  end

  def time_in
  	startTime.strftime("%I:%M%p")
  end

  def time_out
  	endTime.strftime("%I:%M%p")
  end

private
  def task
    @task ||= config[:task_id] && Task.find(config[:task_id])
  end

end
