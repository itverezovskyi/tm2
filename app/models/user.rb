class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  
  has_many :job
  acts_as_token_authenticatable
  attr_accessible :email, :password, :password_confirmation, :role, :name


  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :role, :name
  validates_uniqueness_of :name
end
