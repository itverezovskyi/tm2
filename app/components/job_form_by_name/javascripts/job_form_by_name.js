{
  findJobs: function(){
    var startDate = Ext.getCmp('startDate').getValue();
    var endDate = Ext.getCmp('endDate').getValue();

    if (startDate && startDate.constructor == Date){
      if(endDate && endDate.constructor == Date){
        if(startDate.getDate() < endDate.getDate()){
          this.setRange({"startDate": startDate, "endDate": endDate});
          }else{
            this.getComponent('startDate').markInvalid("End date should be after Start Date");
            this.getComponent('endDate').markInvalid("End date should be after Start Date");
          }
        }else{ this.getComponent('endDate').markInvalid("Wrong Date format"); }
    }else{ this.getComponent('startDate').markInvalid("Wrong Date format"); }
  }
}
