class Jobs < Netzke::Basepack::Grid

  self.edit_inline_available = false

  def configure(c)
    super
    c.model = "Job"
    c.persistence = true
    c.bbar = [:add_in_form, :edit_in_form, :search, "-", :del]
    c.context_menu = nil
    c.columns = [
      {
        :name => :sfid,
        :header => "SFID"
      },
      {
        :name => :client__name,
        :header => "Client"
      },
      :show,
      {
        :name => :booth_size__name,
        :header => "Booth size"
      },
      {
        :name => :project_manager,
        :header => "PM"
      },
      {
        :name => :type,
        :header => "Type",
        :getter => lambda { |r| r.type == 'ShopJob' ? 'Shop job' : 'SFA job'},
        :setter => lambda { |r,v| "Shop job"== v ? r.type = 'ShopJob' : r.type = 'SfaJob' },
        :editor => {
          :xtype => :combobox,
          :store => ['Shop job', 'SFA job']
        }
      }
    ]
  end

  include PgGridTweaks

end
