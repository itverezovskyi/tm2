class JobsByDate < Netzke::Base

  def configure(c)
    super
    c.title = "Jobs by date"
    c.layout = :border
    c.items = [{
      :prevent_header => true,
      :bbar => [:find_jobs_in_range],
      :name => "form",
      :class_name => "Netzke::Basepack::Grid",
      :region => :north,
      :height => 80,
      :title => 'Form',
      :bodyPadding => 10,
      :layout => {
        :type => :hbox,
      },
      :items => [{
        :id => 'startDate',
        :name => :start_date,
        :xtype => :datefield,
        :labelWidth => 70,
        :maxValue => Date.today,
        :allowBlank => :false,
        :fieldLabel => "Start date"
      },{
        :id => 'endDate',
        :name => :end_date,
        :xtype => :datefield,
        :labelWidth => 65,
        :margins => '0 0 0 10',
        :fieldLabel => "End date",
        :maxValue => Date.today,
        :value => Date.today
      }]},
      :jobs_view,
      :tasks
    ]
  end

  js_configure do |c|
    c.mixin
  end

  action :find_jobs_in_range do |c|
    c.handler = :find_jobs
    c.text = "Search"
    c.icon = :find
  end

  component :tasks do |c|
    c.region = :center
    c.split = true
    c.bbar = []
    c.klass = TasksDetails
    c.data_store = {auto_load: false}
    c.scope = lambda {|r| r.where("tasks.id IN #{get_wraped_tasks()}") }
  end

  component :jobs_view do |c|
    c.region = :west
    c.width = 845
    c.split = true
    c.klass = ShopJobsOverviewByDate
    c.data_store = {auto_load:  false}
    c.scope = lambda {|r| r.where("jobs.id IN #{find_jobs()}") }
  end

  endpoint :set_range do |params, this|
    unless params['startDate'].nil? || params['endDate'].nil?
      component_session[:startDate] = params['startDate']
      component_session[:endDate] = params['endDate']
    end
  end

  endpoint :select_job do |params, this|
    component_session[:job_id] = params[:job_id]
  end

  def find_jobs
    find_tasks_in_range.pluck(:job_id).uniq.wrap_to_sql
  end

  def tasks_in_range
    find_tasks_in_range.where(job_id: component_session[:job_id]).where.not(endTime: nil)
  end

  def get_wraped_tasks
    tasks_in_range.pluck(:id).wrap_to_sql
  end

  def find_tasks_in_range
    Task.where('date BETWEEN ? AND ?', component_session[:startDate].to_date, component_session[:endDate].to_date)
  end

end
