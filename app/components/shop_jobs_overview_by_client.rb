class ShopJobsOverviewByClient < JobsOverview

  protected
  
    def client
      @client ||= Netzke::Base.controller.session["netzke_sessions"]["application__jobs_by_client"][:client_id]
    end

    def tasksInScope
      @tasksInScope ||= ShopJob.where(client_id: client).inject(Array.new) {|arr, v| arr.push(v.task)}.each.flat_map {|i| i}
    end

end