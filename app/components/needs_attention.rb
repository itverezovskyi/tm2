class NeedsAttention < Netzke::Basepack::TabPanel

  component :not_synchronized_jobs do |c|
    c.eager_loading = true
  end

  component :automatically_stoped_tasks do |c|
    c.eager_loading = true
  end

  component :system_logs do |c|
    c.eager_loading = true
  end

  def configure(c)
    super
    c.active_tab = 0
    c.prevent_header = true
    c.items = [:not_synchronized_jobs, :automatically_stoped_tasks, :system_logs]
  end

end
