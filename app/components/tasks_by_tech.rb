class TasksByTech < Netzke::Base

  def configure(c)
    super
    c.title = "Tasks by user"
    c.layout = :border
    c.items = [{
      :prevent_header => true,
      :name => "form",
      :class_name => "Netzke::Basepack::Grid",
      :region => :north,
      :height => 60,
      :bodyPadding => 10,
      :layout => {
        :type => :hbox,
      },
      :items => [{
        :id => 'tech',
        :name => :tech,
        :xtype => :combobox,
        :labelWidth => 90,
        :allowBlank => :false,
        :fieldLabel => "Select person",
        :displayField => "name",
        :valueField => "id",
        :lazyRender => true,
        :mode => "local",
        :store => format_for_store(User.where(role: 'Technician'))
      }]},
      :tasks
    ]
  end

  js_configure do |c|
    c.mixin
  end

  component :tasks do |c|
    c.prevent_header = true,
    c.region = :center
    c.border = false
    c.klass = ShopTasksOverview
    c.data_store = {auto_load: false}
    c.scope = lambda {|r| r.where("tasks.user_id = #{component_session[:user_id]}").where.not(endTime: nil) }
  end

  endpoint :select_tech do |params, this|
    component_session[:user_id] = params[:user_id]
  end

private

  def format_for_store(data)
    data.select([:id, :name]).order(:name).map { |x| [x.id, x.name] }
  end

end
