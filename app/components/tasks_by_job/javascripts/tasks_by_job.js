{
  initComponent: function() {
    this.callParent();
    var context = this;

    Ext.getCmp('job').queryMode = 'local';
    Ext.getCmp('job').lastQuery = '';
    Ext.getCmp('job').anyMatch = true;
    Ext.getCmp('job').triggerAction = 'all';

    var view = context.getComponent('tasks').getView();

    Ext.getCmp('client').on('select', function(view, item){

      Ext.getCmp('total').setText("Total time:");
      Ext.getCmp('job').reset();
      context.getComponent('tasks').getStore().removeAll();

      context.getJobs({"client_id": item[0].data.field1}, function(listOfJobs){
        Ext.getCmp('job').store.loadData(listOfJobs, false);
      });
    });

    Ext.getCmp('job').on('select', function(view, item){

      var view = context.getComponent('tasks').getView();
      console.log(item[0].data);

      context.getTotal({"job_id": item[0].data.field1}, function(total){
        Ext.getCmp('total').setText("Total time: " + total);
      });

      context.selectJob({"job_id": item[0].data.field1});
      context.getComponent('tasks').getStore().load();
    })
  }
}
