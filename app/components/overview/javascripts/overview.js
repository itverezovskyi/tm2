{
  initComponent: function() {
    this.callParent();
  },

  onExport: function() {
    function getAllData(store, columns, fileName, title, callback){
      var result = new Array();
      var iterations = 0;
      var numberOfPages = Math.ceil(store.totalCount / store.data.pageSize);

      for (var i = 0; i < numberOfPages; i++) {

        var operation = new Ext.data.Operation({
          action: 'read',
          start : i * store.data.pageSize,
          limit : store.data.pageSize,
          sorters: store.sorters.items
        });

        store.proxy.read(operation, function(res){
          res.resultSet.records.forEach(function(item){
            result.push(item.data);
          })
          if( ++iterations == numberOfPages){
            callback(result, columns, fileName, title);
          }
        });
      }   
    }
  
    function getColumnsToExport(columns){

      var map = new Object();

      columns.forEach(function(item){
        if (!item.hidden){
          map[item.name] = item.text;
        }
      });
      console.log(map);
      return map;
    }

    function parseDate(date){
      var data = new RegExp("(\\d+)..(\\d+).", "").exec(date);
      if (Math.ceil(data[2]/15) != 4){
        return parseFloat(data[1] + "." + Math.ceil(data[2]/15) * 25)
      }else{
        return parseFloat(data[1]) + 1;
      }
    }

    function transformTimeCalculation(data, key){
      var rowToTransform = ["i_and_d_total", "pre_stage_total", "ri_total", "total_time", "time_spended"];

      if (rowToTransform.indexOf(key) != -1){
        data = parseDate(data);
      }

      return data;
    }

    function getRealData(row, key){

      var result;

      if (row.meta.associationValues[key] != undefined){
        result = transformTimeCalculation(row.meta.associationValues[key], key);
      }else{
        result = transformTimeCalculation(row[key], key);
      }

      switch(key){
        case "date":
          result = moment(result).format('L');
          break;
        case "startTime":
          result = moment(result).format('HH:mm A');
          break;
        case "endTime":
          result = moment(result).format('HH:mm A');
          break;
      }

      return result;
    }

    function downloadFile(fileName, urlData) {

      var aLink = document.createElement('a');
      var evt = document.createEvent("HTMLEvents");
      evt.initEvent("click");
      aLink.download = fileName;
      aLink.href = urlData;
      aLink.dispatchEvent(evt);
    }

    function generateHeader(columns){

      var header = "";

      for(var key in columns){
        header += "<th>" + columns[key] + "</th>";
      }

      return header;
    }

    function generateData(data, columns){

      var stringData = "";

      data.forEach(function(item){
        
        var row = '<tr>';

        for(var key in columns){
          row += "<td>" + getRealData(item, key) + "</td>";
        }
      
        stringData += row + "</tr>";
      });

      return stringData;
    }

    function generateTable(data, columns, fileName, title){

      var uri = 'data:application/vnd.ms-excel;base64,';

      var base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) };

      var template = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table><caption><b>{title}</b></caption><thead valign='top'><tr><b>{header}</b></tr></thead><tbody>{data}</tbody></table></body></html>";

      var columnsToExporn = getColumnsToExport(columns);

      template = template.replace("{worksheet}", 'Worksheet');
      template = template.replace("{title}", title);
      template = template.replace("{header}", generateHeader(columnsToExporn));
      template = template.replace("{data}", generateData(data, columnsToExporn));

      downloadFile(fileName + ".xls", uri + base64(template));
    }

    function getNetzkeComponentName(scope){

      var name = "";

      if (typeof scope.netzkeGetParentComponent().title == 'undefined'){
        name = scope.title;
      }else{
        name = scope.netzkeGetParentComponent().title;
      }

      return name;
    }

    function generateFileName(scope){
      return getNetzkeComponentName(scope).toLowerCase().split(' ').join('_') + "___" + moment().format('MM-DD-YYYY___HH:mm');
    }

    function generateTitle(scope){
      return "Report:  <i>" + getNetzkeComponentName(scope) + "</i>   " + moment().format('MMMM Do YYYY, h:mm A');
    }

    getAllData(this.store, this.columns, generateFileName(this), generateTitle(this), generateTable);
    
  }
}
