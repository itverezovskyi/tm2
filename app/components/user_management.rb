class UserManagement < Netzke::Basepack::TabPanel

  component :users do |c|
    c.eager_loading = true
  end

  component :new_user_form do |c|
    c.eager_loading = true
  end

  def configure(c)
    c.active_tab = 0
    c.prevent_header = true
    c.items = [:users, :new_user_form ]
    super
  end

end
