class NotSynchronizedJobs < Jobs
  include Netzke::Basepack::ActionColumn

  self.edit_inline_available = false

  def configure(c)
    super
    c.title = "Not synchronized jobs"
    c.border = false
    c.bbar = [:search]
    c.scope = lambda { |r| r.where("sf_indexes='{}'") }
  end

end
