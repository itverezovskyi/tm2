class SfaJobsOverviewByDate < SfaJobsOverview

protected

  def startDate
    @startDate ||= Netzke::Base.controller.session["netzke_sessions"]["application__sfa_jobs_by_date"][:startDate]
  end

  def endDate
    @endDate ||= Netzke::Base.controller.session["netzke_sessions"]["application__sfa_jobs_by_date"][:endDate]
  end

  def tasksInScope
    @tasksInScope ||= Task.where(date: startDate..endDate).each
  end

end
