class AutomaticallyStopedTasks < Tasks

  self.edit_inline_available = false

  def configure(c)
    super
    c.title = "Automatically stoped tasks"
    c.border = false
    c.bbar = [:search]
    c.scope = lambda { |r| r.where("note like '%STOPPED_AUTOMATICALLY%'") }
  end

end
