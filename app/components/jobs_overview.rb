class JobsOverview < Overview
  include Netzke::Basepack::ActionColumn

  def configure(c)
    super
    c.model = "ShopJob"
    c.prevent_header = true
    c.bbar = (["Admin", "PM"].include? current_user.role) ? [:export] : []
    c.context_menu = nil
    c.columns = [
      {
        :name => :sfid,
        :header => "SFID"
      },
      {
        :name => :client__name,
        :header => "Client"
      },
      :show,
      {
        :name => :pre_stage_total,
        :header => "Pre-stage",
        :getter => lambda {|r| calc_phase_time(r,1)}
      },
      {
        :name => :i_and_d_total,
        :header => "I&D",
        :getter => lambda { |r| calc_phase_time(r,2)}
      },
      {
        :name => :ri_total,
        :header => "RI",
        :getter => lambda { |r| calc_phase_time(r,3)}
      },
      {
        :name => :total_time,
        :header => "Total",
        :getter => lambda { |r| calc_total_time(r)}
      },
      {
        :name => :details,
        :hidden => true
      }
    ]
  end

  include PgGridTweaks

protected

  def current_user
    @current_user ||= Netzke::Base.controller.current_user
  end

  def calc_phase_time(job, phase)
    tasksInScope.each.select {|x| x[:job_id] == job.id and x[:phase_id] == phase and x[:endTime]}.inject(0) {|sum, v| sum+=(v.endTime - v.startTime)}.to_time_difference
  end

  def calc_total_time(job)
    tasksInScope.select {|x| x[:job_id] == job.id and x[:endTime]}.inject(0) {|sum, v| sum+=(v.endTime - v.startTime)}.to_time_difference
  end

end
