class SfaByTech < TasksByTech

  component :tasks do |c|
    c.prevent_header = true,
    c.region = :center
    c.border = false
    c.klass = SfaTasksOverview
    c.data_store = {auto_load: false}
    c.scope = lambda {|r| r.where("tasks.user_id = #{component_session[:user_id]}").where.not(endTime: nil) }
  end

end
