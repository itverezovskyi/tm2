class SfaJobsOverviewByClient < SfaJobsOverview

protected

  def client
    @client ||= Netzke::Base.controller.session["netzke_sessions"]["application__sfa_jobs_by_client"][:client_id]
  end

  def tasksInScope
    @tasksInScope ||= SfaJob.where(client_id: client).inject(Array.new) {|arr, v| arr.push(v.task)}.each.flat_map {|i| i}
  end

end
