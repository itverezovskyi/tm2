class JobsByClient < Netzke::Base

  def configure(c)
    super
    c.title = "Jobs by client"
    c.layout = :border
    c.items = [{
      :prevent_header => true,
      :name => "form",
      :class_name => "Netzke::Basepack::Grid",
      :region => :north,
      :height => 60,
      :bodyPadding => 10,
      :layout => {
        :type => :hbox,
      },
      :items => [{
        :id => 'client',
        :name => :client,
        :xtype => :combobox,
        :labelWidth => 90,
        :allowBlank => :false,
        :fieldLabel => "Select client",
        :displayField => "name",
        :valueField => "id",
        :lazyRender => true,
        :mode => "local",
        :store => Client.select([:id, :name]).order(:name).map { |x| [x.id, x.name] }
      }]},
      :jobs,
      :tasks
    ]
  end

  js_configure do |c|
    c.mixin
  end

  component :jobs do |c|
    c.region = :west
    c.width = 845
    c.split = true
    c.klass = ShopJobsOverviewByClient
    c.data_store = {auto_load: false}
    c.scope = lambda { |r| r.where("client_id = #{component_session[:client_id]}") }
  end

  component :tasks do |c|
    c.region = :center
    c.split = true
    c.bbar = []
    c.klass = TasksDetails
    c.data_store = {auto_load: false}
    c.scope = lambda { |r| r.where("job_id = #{component_session[:job_id]}").where.not(endTime: nil) }
  end

  endpoint :select_client do |params, this|
    component_session[:client_id] = params[:client_id]
  end

  endpoint :select_job do |params, this|
    component_session[:job_id] = params[:job_id]
  end

end
