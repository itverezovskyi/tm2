class NewUserForm < Netzke::Basepack::Form

  def configure(c)
    super
    c.model = "User"
    c.title = "Crete new User"
    c.width = 400
    c.height = 300
    c.items = [
      :name,
      :email,
      :password,
      :password_confirmation,
      {:name => :role, :xtype => :combo, :store => ['Admin', 'PM', 'Technician']}
    ]
  end

  endpoint :netzke_submit do |params, this|

    data = ActiveSupport::JSON.decode(params[:data])
    res = []
    success = false

    @user = User.new(
      :name => data["name"],
      :email => data["email"],
      :password => data["password"],
      :password_confirmation => data["password_confirmation"],
      :role => data["role"]
    )

    if success = @user.save
      res << "Saved !"
    end
       
    { :set_result => success }

    this.netzke_feedback @user.errors.full_messages | res

  end

end