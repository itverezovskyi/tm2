class Users < Netzke::Basepack::Grid

  self.edit_inline_available = false

  def configure(c)
    super
    c.model = "User"
    c.persistence = true
    c.bbar = [:edit_in_form, :add_new, "-", :del]
    c.context_menu = nil
    c.columns = [
      :name,
      :email,
      {
        :name => :role,
        :editor => {
          :xtype => :combobox,
          :store => ['Admin', 'PM', 'Technician']
        }
      }
    ]
  end

  include PgGridTweaks

end
