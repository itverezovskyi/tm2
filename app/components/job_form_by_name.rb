class JobFormByName < Netzke::Basepack::Form

  def configure(c)
    super
    c.bbar = [:find_jobs_in_range]
    c.layout = {
        :type => :hbox
      }
    c.border = false
    c.items = [{
        :id => 'startDate',
        :name => :start_date,
        :xtype => :datefield,
        :labelWidth => 70,
        :maxValue => Date.today,
        :allowBlank => :false
      },{
        :id => 'endDate',
        :name => :end_date,
        :xtype => :datefield,
        :labelWidth => 65,
        :margins => '0 0 0 10',
        :maxValue => Date.today
      }
    ]
  end

  js_configure do |c|
    c.mixin
  end

  action :find_jobs_in_range do |c|
    c.handler = :find_jobs
    c.text = "Search"
    c.icon = :find
  end

  endpoint :set_range do |params, this|
    unless params['startDate'].nil? || params['endDate'].nil?
      session[:startDate] = params['startDate']
      session[:endDate] = params['endDate']
      this.set_title("Date search: #{Date.parse params['startDate']} - #{Date.parse params['endDate']}")
    end
  end

end
