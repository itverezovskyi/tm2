class StaticGrid < Netzke::Basepack::Grid

  def configure(c)
    super
    c.persistence = true
    c.enable_edit_in_form = false
    c.enable_edit = false
    c.edit_inline_available = false
    c.prohibit_delete = true
    c.prohibit_create = true
    c.prohibit_update = true
    c.context_menu = nil
  end
end
