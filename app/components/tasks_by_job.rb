class TasksByJob < Netzke::Base

  def configure(c)
    super
    c.title = "Tasks by job"
    c.border = false
    c.items =[{
      :prevent_header => true,
      :name => "form",
      :class_name => "Netzke::Basepack::Grid",
      :region => :north,
      :height => 60,
      :bodyPadding => 10,
      :bodyBorder => 10,
      :layout => {
        :type => :hbox,
      },
      :items => [{
        :id => 'client',
        :name => :client,
        :xtype => :combobox,
        :labelWidth => 40,
        :allowBlank => :false,
        :fieldLabel => "Client",
        :displayField => "name",
        :valueField => "id",
        :lazyRender => true,
        :store => Client.select([:id, :name]).order(:name).map { |x| [x.id, x.name] }
      },{
        :id => 'job',
        :name => :job,
        :xtype => :combobox,
        :labelWidth => 25,
        :margin => "0 0 0 10",
        :allowBlank => :false,
        :fieldLabel => "Job",
        :displayField => "sfid",
        :valueField => "id",
        :lazyRender => true,
        :mode => "local",
        :store => [["0","0"]]
      },{
        :id => 'total',
        :name => :total,
        :xtype => :label,
        :labelWidth => 25,
        :margin => "5 0 0 50",
        :allowBlank => :false,
        :fieldLabel => "Total: ",
        :displayField => "total",
        :lazyRender => true,
        :mode => "local",
        :text => "Total time"
      }
      ]},
      :tasks
      ]
  end

  js_configure do |c|
    c.mixin
  end

  component :tasks do |c|
    c.prevent_header = true
    c.region = :center
    c.border = false
    c.klass = TasksOverview
    c.data_store = {auto_load: false}
    c.scope = lambda { |r| r.where("job_id = #{component_session[:job_id]}").where.not(endTime: nil) }
  end

  endpoint :get_clients do |params, this|
    this.netzke_set_result(Client.select("id, name").load)
  end

  endpoint :get_jobs do |params, this|
    this.netzke_set_result(select_for_store(ShopJob.where(client_id: params[:client_id])))
  end

  endpoint :select_client do |params, this|
    component_session[:client_id] = params[:client_id]
  end

  endpoint :select_job do |params, this|
    component_session[:job_id] = params[:job_id]
  end

  endpoint :get_total do |params, this|
    this.netzke_set_result(ShopJob.calculate_total(params[:job_id]))
  end

private

  def select_for_store(data)
    create_hash(data.select(:id, :sfid).order(:sfid))
  end

  def create_hash(data)
    data.map { |item| Hash.[]("field1", item[:id], "field2", item[:sfid]) }
  end

end
