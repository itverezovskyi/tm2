class MainMenu < Netzke::Base

  def configure(c)
    c.store = { root: menu }
    super
  end

  js_configure do |c|
    c.extend = "Ext.tree.Panel"
  end

  protected

  def current_user
    @current_user ||= Netzke::Base.controller.current_user
  end

  def leaf(text, component, icon = nil)
    { text: text,
      icon: icon && uri_to_icon(icon),
      cmp: component,
      leaf: true
    }
  end

  def menu
    out = {
      :text => "Navigation",
      :expanded => true,
      :children => [shop, sfa]
    }

    out[:children] << mangement if current_user.role == "Admin"

    out
  end

  def shop
    {
      :text => "Shop Jobs",
      :expanded => true,
      :children => [
        leaf("By date", :jobs_by_date, :user),
        leaf("By tech", :tasks_by_tech, :user),
        leaf("By job", :tasks_by_job, :user),
        leaf("By client", :jobs_by_client, :user)
      ]
    }
  end

  def sfa
    {
      :text => "SFA",
      :expanded => false,
      :children => [
        leaf("By date", :sfa_jobs_by_date, :user),
        leaf("By tech", :sfa_by_tech, :user),
        leaf("By job", :sfa_by_job, :user),
        leaf("By client", :sfa_jobs_by_client, :user)
      ]
    }
  end

  def mangement
    {
      :text => "Management",
      :expanded => false,
      :children => [
        leaf("Manage Jobs", :jobs, :user),
        leaf("Manage Times", :tasks, :user),
        leaf("Manage Clients", :clients, :user),
        leaf("Manage Booth Sizes", :booth_sizes, :user),
        leaf("Manage User", :user_management, :user),
        leaf("Needs Attention", :needs_attention, :user),
        leaf("Phases", :phases, :user)
      ]
    }
  end

end
