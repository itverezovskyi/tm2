{
  initComponent: function() {
    this.callParent();

    this.getComponent('jobs_view').getView().on('itemclick', function(view, record){
      this.selectJob({job_id: record.get('id')});
      this.getComponent('tasks').getStore().load();
    }, this);
  },

  findJobs: function(){
    this.getComponent('tasks').getStore().removeAll();
    var startDate = Ext.getCmp('startDate').getValue();
    var endDate = Ext.getCmp('endDate').getValue();

    if (startDate && startDate.constructor == Date){
      if(endDate && endDate.constructor == Date){
        if(startDate < endDate){
          this.setRange({"startDate": startDate, "endDate": endDate});
          }else{
            Ext.getCmp('startDate').markInvalid("End date should be after Start Date");
            Ext.getCmp('endDate').markInvalid("End date should be after Start Date");
          }
        }else{ Ext.getCmp('endDate').markInvalid("Wrong Date format"); }
    }else{ Ext.getCmp('startDate').markInvalid("Wrong Date format"); }

    this.getComponent('jobs_view').getStore().load();
  }
}
