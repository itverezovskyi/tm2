class Application < Netzke::Basepack::Viewport

  action :sign_in do |c|
    c.icon = :door_in
  end

  action :sign_out do |c|
    c.icon = :door_out
    c.text = "Logout #{current_user.name}" if current_user
  end

  js_configure do |c|
    c.layout = :fit
    c.mixin
  end

  component :main_menu do |c|
    c.klass = MainMenu
    c.title = "Menu"
    c.collapsible = true
    c.root_visible = false
    c.split = true
    c.width = 230
    c.region = :west
  end

  def configure(c)
    super
    c.items = [
      { layout: :border,
        tbar: [header_html, '->', current_user ? :sign_out : :sign_in],
        items: [
          :main_menu,
          { region: :center, layout: :border, border: false, items: [
            { item_id: :main_panel, region: :center, layout: :fit, border: false, items: [{body_padding: 5, html: "Components will be loaded in this area"}] }
          ]}
        ]
      }
    ]
  end

  # Endpoints
  #
  #

  endpoint :sign_out do |params, this|
    redirect_to "logout"
  end

  #
  # Main Area
  #

  component :jobs_by_date do |c|
  end

  component :tasks_by_tech do |c|
  end

  component :tasks_by_job do |c|
  end

  component :jobs_by_client do |c|
  end

  #
  # SFA Area
  #

  component :sfa_jobs_by_client do |c|
  end

  component :sfa_by_job do |c|
  end

  component :sfa_by_tech do |c|
  end

  component :sfa_jobs_by_date do |c|
  end

  #
  # Managment
  #

  component :jobs do |c|
  end

  component :tasks do |c|
  end

  component :clients do |c|
  end

  component :booth_sizes do |c|
  end

  component :phases do |c|
  end

  component :tasks do |c|
  end

  component :user_management do |c|
  end

  component :users do |c|
  end

  component :new_user_form do |c|
  end

  component :needs_attention do |c|
  end

  component :not_synchronized_jobs do |c|
  end

  component :automatically_stoped_tasks do |c|
  end

  component :system_logs do |c|
  end

protected

  def current_user
    @current_user ||= Netzke::Base.controller.current_user
  end

  def header_html
    %Q{
      <div style="color:#333; font-family: Helvetica; font-size: 150%;">
        Time machine test
      </div>
    }
  end

end
