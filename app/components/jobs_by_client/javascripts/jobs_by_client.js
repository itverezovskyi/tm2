{
  initComponent: function() {
    this.callParent();

    var context = this;

    this.getComponent('jobs').getView().on('itemclick', function(view, record){
      this.selectJob({job_id: record.get('id')});
      this.getComponent('tasks').getStore().load();
    }, this);

    Ext.getCmp('client').on('select', function(view, item){
      context.getComponent('jobs').getStore().removeAll();
      context.selectClient({"client_id": this.getValue()});
      context.getComponent('jobs').getStore().load();
    });
  }
}
