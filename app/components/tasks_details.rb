class TasksDetails < StaticGrid

  self.edit_inline_available = false

  def configure(c)
    super
    c.model = "Task"
    c.bbar = []
    c.columns = [
      :date,
      {
        :name => :job__sfid,
        :header => "SFID"
      },
      {
        :name => :phase__name,
        :header => "Phase"
      },
      {
        :name => :user__name,
        :header => "Tech"
      },
      {
        :name => :startTime,
        :header => "Time in",
        :format => "g:i A"
      },
      {
        :name => :endTime,
        :header => "Time out",
        :format => "g:i A"
      },
      {
        :name => :time_spended,
        :header => "Total",

      },
      :note
    ]
  end

  include PgGridTweaks
end
