class Clients < Netzke::Basepack::Grid

  self.edit_inline_available = false

  def configure(c)
    super
    c.model = "Client"
    c.persistence = true
    c.bbar = [:add_in_form, :edit_in_form, :search, "-", :del]
    c.context_menu = nil
    c.columns = [
      :name
    ]
  end

  include PgGridTweaks

end
