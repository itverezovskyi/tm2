class SfaJobsByDate < JobsByDate

  component :jobs_view do |c|
    c.region = :west
    c.width = 845
    c.split = true
    c.klass = SfaJobsOverviewByDate
    c.data_store = {auto_load:  false}
    c.scope = lambda {|r| r.where("jobs.id IN #{find_jobs()}") }  
  end
  
end
