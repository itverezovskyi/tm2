class TasksOverview < Overview

  def configure(c)
    super
    c.model = "Task"
    c.bbar =  (["Admin", "PM"].include? current_user.role) ? [:export] : []
    c.context_menu = nil
    c.columns = [
      :date,
      {
        :name => :job__sfid,
        :header => "SFID"
      },
      {
        :name => :phase__name,
        :header => "Phase"
      },
      {
        :name => :user__name,
        :header => "Tech"
      },
      {
        :name => :job__client__name,
        :header => "Client"
      },
      {
        :name => :startTime,
        :header => "Time in",
        :format => 'g:i A',
        :width => 100

      },
      {
        :name => :endTime,
        :header => "Time out",
        :format => 'g:i A',
        :width => 100
      },
      {
        :name => :time_spended,
        :header => "Total",

      },
      :note
    ]
  end

  include PgGridTweaks

  protected

  def current_user
    @current_user ||= Netzke::Base.controller.current_user
  end
end
