class Overview < StaticGrid

  action :export do |c|
    c.text = "Export"
    c.icon = :page_excel
  end

  def configure(c)
    super
  end

  js_configure do |c|
    c.mixin
  end

end
