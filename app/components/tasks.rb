class Tasks < Netzke::Basepack::Grid

  self.edit_inline_available = false

  def configure(c)
    super
    c.model = "Task"
    c.persistence = true
    c.bbar = [:add_in_form, :edit_in_form, :search, "-", :del]
    c.context_menu = nil
    c.columns = [
      :date,
      {
        :name => :job__sfid,
        :header => "SFID"
      },
      {
        :name => :phase__name,
        :header => "Phase"
      },
      {
        :name => :user__name,
        :header => "Tech"
      },
      {
        :name => :startTime,
        :header => "Time in",
        :format => "g:i A"
      },
      {
        :name => :endTime,
        :header => "Time out",
        :format => "g:i A"
      },
      :note
    ]
  end

  include PgGridTweaks

end
