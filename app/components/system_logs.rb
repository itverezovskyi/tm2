class SystemLogs < StaticGrid

  def configure(c)
    super
    c.model = "SystemLog"
    c.title = "System log"
    c.bbar = [:search]
    c.columns = [
      {
        :name => :created_at,
        :header => "Date",
        :format => "m/d/Y"
      },
      :level,
      :job,
      :action
    ]
  end


  include PgGridTweaks
end
