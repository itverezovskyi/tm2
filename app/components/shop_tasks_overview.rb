class ShopTasksOverview < TasksOverview
  
  def configure(c)
    super
    c.model = "ShopTask"
  end

end