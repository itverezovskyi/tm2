class SfaJobsByClient < JobsByClient

  component :jobs do |c|
    c.region = :west
    c.width = 845
    c.split = true
    c.klass = SfaJobsOverviewByClient
    c.data_store = {auto_load: false}
    c.scope = lambda {|r| r.where("client_id = #{component_session[:client_id]}") } 
  end
  
end