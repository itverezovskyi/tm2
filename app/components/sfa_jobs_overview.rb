class SfaJobsOverview < JobsOverview

  def configure(c)
    super
    c.model = "SfaJob"
  end

end