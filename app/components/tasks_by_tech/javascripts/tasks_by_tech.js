{
  initComponent: function() {
    this.callParent();

    var context = this;

    Ext.getCmp('tech').on('select', function(view, item){
      context.getComponent('tasks').getStore().removeAll();
      context.selectTech({"user_id": this.getValue()});
      context.getComponent('tasks').getStore().load();
    });
  }
}
