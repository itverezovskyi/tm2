class SfaByJob < TasksByJob

  component :tasks do |c|
    c.prevent_header = true,
    c.region = :center
    c.border = false
    c.klass = SfaTasksOverview
    c.data_store = {auto_load: false}
    c.scope = lambda {|r| r.where("job_id = #{component_session[:job_id]}").where.not(endTime: nil) }
  end

  endpoint :get_jobs do |params, this|
    this.netzke_set_result(select_for_store(SfaJob.where(client_id: params[:client_id])))
  end

  endpoint :get_total do |params, this|
    this.netzke_set_result(SfaJob.calculate_total(params[:job_id]))
  end
end
