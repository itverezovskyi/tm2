class ShopJobsOverview < JobsOverview

  def configure(c)
    super
    c.model = "ShopJob"
  end

end