require 'mailgun'

class AppMailer < ApplicationMailer

  def stoped_task_notification(task)
    @task = task
    @user = task.user
    @job  = task.job

    mg_client = Mailgun::Client.new ENV["MAILGUN_API_KEY"]
    msg = Mailgun::MessageBuilder.new

    msg.set_from_address(ENV["MAILGUN_EMAIL"], {"first" => ENV["MAILGUN_SENDER_NAME"]});
    msg.add_recipient(:to, @user.email, {"first" => @user.name});
    msg.set_subject("#{ENV["MAILGUN_SENDER_NAME"]} notification");
    msg.set_html_body((render_to_string(template: "../views/app_mailer/stoped_task_notification")).to_str);

    if (ENV["CC_EMAILS"]) && (ENV["CC_EMAILS"] != '')
      ENV["CC_EMAILS"].split(";").each do |email|
        # msg.add_recipient(:bcc, email, {"first" => email.split('@')[0].capitalize})
      end
    end

    mg_client.send_message(ENV["MAILGUN_DOMAIN"], msg)
  end

end
