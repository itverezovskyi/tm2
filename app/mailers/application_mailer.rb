class ApplicationMailer < ActionMailer::Base
  default from: ENV["MAILGUN_EMAIL"]
  layout 'mailer'
end
