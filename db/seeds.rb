Phase.create([
  {name: "PreStage"},
  {name: "I&D"},
  {name: "Return inspection"}
])

Client.create([
  {name: "NOT SPECIFIED"}
])

BoothSize.create([
  {name: "NOT SPECIFIED"}
])

ShopJob.create([
  {client_id: 1, sfid: "0", show: "NOT SPECIFIED", booth_size_id: 1, project_manager: "NOT SPECIFIED"}
])

SfaJob.create([
  {client_id: 1, sfid: "0", show: "NOT SPECIFIED", booth_size_id: 1, project_manager: "NOT SPECIFIED"}
])

User.create([
  {email: "tm_admin@gmail.com", name: "Main admin", password: "12wqKSd_34**16", password_confirmation: "12wqKSd_34**16", role: "Admin"}
])

SfIntegration.create([
  {last_update_time: DateTime.new(2000, 1, 1, 1, 1, 1)}
])
