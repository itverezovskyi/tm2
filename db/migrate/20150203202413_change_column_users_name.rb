class ChangeColumnUsersName < ActiveRecord::Migration
  def change
  	change_column_null :users, :name, false
  	change_column_default :users, :name, ""
  end
end
