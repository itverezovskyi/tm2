class ChangeColumnUsersRole < ActiveRecord::Migration
  def change
  	change_column_null :users, :role, false
  	change_column_default :users, :role, ""
  end
end
