class RemoveOnTabletFromTasks < ActiveRecord::Migration
  def change
    remove_column :tasks, :onTablet, :boolean
  end
end
