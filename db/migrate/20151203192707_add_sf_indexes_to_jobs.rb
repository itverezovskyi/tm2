class AddSfIndexesToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :sf_indexes, :text, array:true, default: []
  end
end
