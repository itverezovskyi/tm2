class RenameTypeInSystemLog < ActiveRecord::Migration
  def change
    rename_column :system_logs, :type, :level
  end
end
