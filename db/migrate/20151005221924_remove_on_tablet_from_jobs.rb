class RemoveOnTabletFromJobs < ActiveRecord::Migration
  def change
    remove_column :jobs, :onTablet, :boolean
  end
end
