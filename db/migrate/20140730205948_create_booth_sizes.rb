class CreateBoothSizes < ActiveRecord::Migration
  def change
    create_table :booth_sizes do |t|
      t.string :name

      t.timestamps
    end
  end
end
