class CreateSfIntegrations < ActiveRecord::Migration
  def change
    create_table :sf_integrations do |t|
      t.string :oauth_token
      t.string :refresh_token
      t.string :instance_url
      t.string :client_id
      t.string :client_secret
      t.datetime :last_update_time

      t.timestamps
    end
  end
end
