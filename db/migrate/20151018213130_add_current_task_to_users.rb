class AddCurrentTaskToUsers < ActiveRecord::Migration
  def change
    add_column :users, :current_task, :integer
  end
end
