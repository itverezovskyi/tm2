class CreateTasks < ActiveRecord::Migration

  def change
    create_table :tasks do |t|
      t.date :date
      t.datetime :startTime
      t.datetime :endTime
      t.references :job, index: true
      t.references :user, index: true
      t.references :phase, index: true
      t.string :note
      t.boolean :onTablet, :default => true
      t.string :type

      t.timestamps
    end
  end


end
