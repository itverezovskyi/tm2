class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.references :client, index: true
      t.string :sfid, uniq: true
      t.string :show
      t.references :booth_size, index: true
      t.string :project_manager
      t.boolean :onTablet, :default => true
      t.string :type

      t.timestamps
    end

    add_index :jobs, :sfid, unique: true

  end
end
