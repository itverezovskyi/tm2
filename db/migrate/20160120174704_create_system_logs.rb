class CreateSystemLogs < ActiveRecord::Migration
  def change
    create_table :system_logs do |t|
      t.string :type
      t.string :job
      t.text :action

      t.timestamps null: false
    end
  end
end
