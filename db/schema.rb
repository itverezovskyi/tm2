# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160120182829) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "booth_sizes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "account"
  end

  create_table "jobs", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "sfid"
    t.string   "show"
    t.integer  "booth_size_id"
    t.string   "project_manager"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "sf_indexes",      default: [], array: true
  end

  add_index "jobs", ["booth_size_id"], name: "index_jobs_on_booth_size_id", using: :btree
  add_index "jobs", ["client_id"], name: "index_jobs_on_client_id", using: :btree
  add_index "jobs", ["sfid"], name: "index_jobs_on_sfid", unique: true, using: :btree

  create_table "phases", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "schema_info", id: false, force: :cascade do |t|
    t.integer "version", default: 0, null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "sf_integrations", force: :cascade do |t|
    t.string   "oauth_token"
    t.string   "refresh_token"
    t.string   "instance_url"
    t.string   "client_id"
    t.string   "client_secret"
    t.datetime "last_update_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_logs", force: :cascade do |t|
    t.string   "level"
    t.string   "job"
    t.text     "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.date     "date"
    t.datetime "startTime"
    t.datetime "endTime"
    t.integer  "job_id"
    t.integer  "user_id"
    t.integer  "phase_id"
    t.string   "note"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tasks", ["job_id"], name: "index_tasks_on_job_id", using: :btree
  add_index "tasks", ["phase_id"], name: "index_tasks_on_phase_id", using: :btree
  add_index "tasks", ["user_id"], name: "index_tasks_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role",                   default: "", null: false
    t.string   "name",                   default: "", null: false
    t.string   "authentication_token"
    t.integer  "current_task"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["name"], name: "index_users_on_name", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
