class Float
  def to_time_difference
    hours = (self / 3600).floor
    minutes = ((self - (hours * 3600)) / 60).floor
    "#{hours}h #{minutes}m"
  end
end