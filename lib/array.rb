class Array
  def wrap_to_sql
    if (self.empty?)
      "(-1)"
    else
      "('#{self.join("','")}')"
    end
  end
end