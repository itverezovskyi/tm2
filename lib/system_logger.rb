module SystemLogger
  def self.info(job, action)
    SystemLog.new(level: "Info", job: job, action: action).save!
  end

  def self.error(job, action)
    SystemLog.new(level: "Error", job: job, action: action).save!
  end
end
